package in.co.parque.task.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Spinner;

import java.util.ArrayList;

import in.co.parque.task.adapter.Spinner_Adapter;
import in.co.parque.task.object_class.Option_Object;

/**
 * Created by parqueandroid on 23/1/18.
 */

@SuppressLint("AppCompatCustomView")
public class Spinner_CustomView  {

    Spinner spinner ;
    Context context ;

    public Spinner_CustomView(Context context) {
        this.context = context;
    }

    public Spinner getSpinner() {

        spinner = new Spinner(context);
        spinner.setPadding(20,20,20,20);
        return spinner ;
    }

    public Spinner_Adapter getAdapter(int resId, ArrayList<Option_Object> arrayList) {
        return new Spinner_Adapter(context, resId, arrayList);
    }

}
