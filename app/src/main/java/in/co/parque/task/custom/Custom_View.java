package in.co.parque.task.custom;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import in.co.parque.task.adapter.MutlSelect_Adapter;
import in.co.parque.task.R;
import in.co.parque.task.object_class.Option_Object;

/**
 * Created by parqueandroid on 23/1/18.
 */

public class Custom_View extends LinearLayout implements DatePickerDialog.OnDateSetListener {



    public TextView questionTextView;
    public TextView labelTextView;
    public EditText answerEditText;
    public  LayoutParams layoutParams;
    Calendar myCalendar;




    public Custom_View(Context context) {
        super(context);
        init(context);
    }



    private void init(Context context) {
        setOrientation(1);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        this.labelTextView = new TextView(context);
        float f = getResources().getDisplayMetrics().density;
        int i = (int) ((30.0f * f) + 0.5f);
        this.labelTextView.setHeight(i);
        this.labelTextView.setWidth(i);
        this.labelTextView.setGravity(17);
        this.layoutParams = new LayoutParams(-1, -2);
        if (Build.VERSION.SDK_INT >= 21) {
            this.labelTextView.setBackground(context.getResources().getDrawable(R.drawable.texview_design, null));
        }
        this.labelTextView.setText("1");
        this.questionTextView = new TextView(context);
        this.questionTextView.setPadding((int) ((8.0f * f) + 0.5f), 0, 0, 0);
        this.layoutParams.setMargins(4, 1, 4, 4);
        this.questionTextView.setGravity(16);
        this.questionTextView.setTextColor(getResources().getColor(R.color.black));
        linearLayout.addView(this.labelTextView);
        ViewGroup.LayoutParams layoutParams = new LayoutParams(-1, -2);//0,-2
        this.questionTextView.setLayoutParams(layoutParams);
        linearLayout.addView(this.questionTextView);
        linearLayout.setLayoutParams(new LayoutParams(-1, -2));
        addView(linearLayout);
        this.answerEditText = new EditText(context);
        addView(this.answerEditText, this.layoutParams);
        linearLayout.invalidate();
    }


    public void setQuestion(String question){

        this.questionTextView.setText(question);
    }


    public void setOrder(String str) {
        this.labelTextView.setText(str);
        this.labelTextView.invalidate();
    }


    public void setAnswerVisibility(int visibility){

        this.answerEditText.setVisibility(visibility);
    }


    public void setInputType(){

        this.answerEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setDate(){

      myCalendar = Calendar.getInstance();
        this.answerEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus){
                    new DatePickerDialog(getContext(),Custom_View.this , myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


        SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        this.answerEditText.setText(sdformat.format(myCalendar.getTime()));

    }

    public void multiSelectionMethod(final ArrayList<Option_Object> arrayList){

            this.answerEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    if (hasFocus){
                        open_alertDialog(arrayList);
                    }
                }
            });
    }


    public void open_alertDialog(ArrayList<Option_Object> arrayList){

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView;
        final AlertDialog alertDialog;
        dialogView = inflater.inflate(R.layout.dialog_layout, null);

        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.rvList);
        Button cancle = (Button) dialogView.findViewById(R.id.btnCancel);
        Button done = (Button) dialogView.findViewById(R.id.btnBook);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        final MutlSelect_Adapter adapter = new MutlSelect_Adapter(getContext(),arrayList);
        recyclerView.setAdapter(adapter);

        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();

        done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> list2 = adapter.getChecked_List();
                StringBuilder builder = new StringBuilder();
                for (String details : list2) {
                    builder.append(details + ",");
                }

                answerEditText.setText(builder.toString());
                alertDialog.dismiss();

            }
        });

        cancle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
            }
        });

    }


}
