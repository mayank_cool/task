package in.co.parque.task.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import in.co.parque.task.R;
import in.co.parque.task.object_class.Option_Object;

/**
 * Created by parqueandroid on 24/1/18.
 */

public class MutlSelect_Adapter extends RecyclerView.Adapter<MutlSelect_Adapter.ViewHolder_Class> {

     ArrayList<Option_Object> arrayList ;
    Context context ;
    ArrayList<String> check_list = new ArrayList<>();


    public MutlSelect_Adapter(Context context,ArrayList<Option_Object> arrayList){
        this.context = context ;
        this.arrayList = arrayList ;

    }

    @Override
    public ViewHolder_Class onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.multiselect_adapter_layout, viewGroup, false);
       ViewHolder_Class viewHolder_class = new ViewHolder_Class(v);

        return viewHolder_class;
    }

    @Override
    public void onBindViewHolder(ViewHolder_Class holder, int position) {

        final Option_Object obj = arrayList.get(position) ;
        holder.textView.setText(obj.value);
        holder.checkBox.setChecked(obj.isSelected);
        holder.checkBox.setTag(obj);
        if (obj.isSelected){

            check_list.add(obj.value);
        }

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckBox cb = (CheckBox) view;
                Option_Object obj1 = (Option_Object) cb.getTag();

                obj1.isSelected = cb.isChecked();
                String value = obj.value;
                if (obj1.isSelected ){

                    check_list.add(value);

                }else {
                    check_list.remove(value);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder_Class extends RecyclerView.ViewHolder{

        TextView textView ;
        CheckBox checkBox ;

        public ViewHolder_Class(View itemview){

            super(itemview);

            textView = (TextView) itemview.findViewById(R.id.textValue);
            checkBox = (CheckBox) itemview.findViewById(R.id.chkBox);

        }
    }

    public ArrayList<String> getChecked_List() {
        return check_list;
    }
}
