package in.co.parque.task;

import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import in.co.parque.task.adapter.Spinner_Adapter;
import in.co.parque.task.custom.Custom_View;
import in.co.parque.task.custom.Spinner_CustomView;
import in.co.parque.task.object_class.Option_Object;
import in.co.parque.task.object_class.Form_data;

import static android.widget.RelativeLayout.*;

/**
 * Created by parqueandroid on 22/1/18.
 */

public class Runtime_Form extends AppCompatActivity{


    LinearLayout linearLayout  ;
    ArrayList<Form_data> arrayList ;
    ConnectionDetector cd ;
    Boolean isInternetPresent = false;
    AlertDialog dialog ;
    public static final String url = "https://stgdreamafrica.dhwaniris.in/index.php/v1/task/new-task" ;
    public String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJ1c2VyX2xldmVsIjoiMyIsInVzZXJfdHlwZSI6IjIiLCJuYW1lIjoiU1NTU1MiLCJtb2JpbGVfbnVtYmVyIjoiMjE1NDUxMjEyMSIsInBhc3N3b3JkX3Jlc2V0X2F0IjpudWxsLCJ2ZXJzaW9uIjpudWxsLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6bnVsbCwiY3JlYXRlZF9hdCI6IjIwMTctMDgtMDQgMTk6MDI6MjgiLCJsb2dpbl9pZCI6NzF9.uDCPTQn45nnlcQvJWEO3cD7GRhwsa0MRxVRmOIm1D-A";



    @Override
     protected void onCreate(Bundle savedInstanceState){
      super.onCreate(savedInstanceState);
      setContentView(R.layout.runtime_form);
      arrayList = new ArrayList<>();
      linearLayout = (LinearLayout) findViewById(R.id.linear);
      cd = new ConnectionDetector(this);
      isInternetPresent = cd.isConnectingToInternet();
      if (isInternetPresent){
          getDataFromServer();
      }else {
          Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
      }
  }

  public void getDataFromServer(){

      String tag_json_obj = "json_obj_req";
      dialog = new SpotsDialog(this);
      dialog.show();
      final JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,
              null, new Response.Listener<JSONObject>() {

          @Override
          public void onResponse(JSONObject response) {

              if(dialog != null){
                  if(dialog.isShowing()){
                      dialog.dismiss();
                  }
              }

              try {
                  boolean status = response.getBoolean("status");

                  if (status){

                      JSONArray data = response.getJSONArray("data");

                      for (int i=0 ; i < data.length(); i++){

                          JSONObject obj = data.getJSONObject(i);
                          Form_data form_data = new Form_data();

                          form_data.name = obj.getString("name");
                          form_data.label = obj.getString("label");
                          form_data.type = obj.getString("type");

                          form_data.arrayList = new ArrayList<>();
                          JSONArray options = obj.getJSONArray("options");

                          if (options.length() > 0){

                              for (int j=0; j< options.length();j++){

                                  Option_Object optionObject = new Option_Object();
                                  JSONObject object = options.getJSONObject(j);

                                  optionObject.id = object.getString("id");
                                  optionObject.value = object.getString("value");

                                  form_data.arrayList.add(optionObject);
                              }

                          }

                          arrayList.add(form_data);
                      }
                      intialize_view();
                  }

              } catch (JSONException e) {
                  e.printStackTrace();
              }


          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
              Log.e("TAG","error res");
              if(dialog != null){
                  if(dialog.isShowing()){
                      dialog.dismiss();
                  }
              }
          }
      }) {

          /**
           * Passing some request headers
           */
          @Override
          public Map<String, String> getHeaders() throws AuthFailureError {
              HashMap<String, String> headers = new HashMap<String, String>();
              headers.put("Content-Type", "application/json");
              headers.put("token", token);
              Log.e("TAG","header" + headers.toString());
              return headers;
          }
      };
      YourApplication.getInstance().addToRequestQueue(req, tag_json_obj);
  }



  @RequiresApi(api = Build.VERSION_CODES.N)
  public void intialize_view(){

      for (int i=0; i<arrayList.size(); i++){

          final Form_data data = arrayList.get(i);

          if (data.type.equals("1")){

              Custom_View view = new Custom_View(this);
              view.labelTextView.setText(data.label);
              view.answerEditText.setHint("Enter "+ data.name);
              view.setQuestion(data.name);
              linearLayout.addView(view);

          }else if (data.type.equals("2")){
              Custom_View view = new Custom_View(this);
              view.setOrder(data.label);
              view.answerEditText.setHint("Enter "+ data.name);
              view.setInputType();
              view.setQuestion(data.name);
              linearLayout.addView(view);

          }else if (data.type.equals("3")){

              Custom_View view = new Custom_View(this);
              view.setOrder(data.label);
              view.setQuestion(data.name);
              view.setAnswerVisibility(GONE);
              linearLayout.addView(view);
              Spinner_CustomView spinner_View = new Spinner_CustomView(Runtime_Form.this);
              Spinner spin = spinner_View.getSpinner();
              Spinner_Adapter adapter = spinner_View.getAdapter(android.R.layout.simple_spinner_item,data.arrayList);
              spin.setAdapter(adapter);
              linearLayout.addView(spin);

          }else if (data.type.equals("4")){

              Custom_View view = new Custom_View(this);
              view.setOrder(data.label);
              view.setQuestion(data.name);
              view.answerEditText.setHint("Select "+ data.name);
              view.multiSelectionMethod(data.arrayList);
              linearLayout.addView(view);

          }else if (data.type.equals("5")){

              Custom_View view = new Custom_View(this);
              view.setOrder(data.label);
              view.setQuestion(data.name);
              view.answerEditText.setHint("Enter "+ data.name);
              view.setDate();
              linearLayout.addView(view);
          }
      }
  }

}
