package in.co.parque.task.object_class;

import java.util.ArrayList;

/**
 * Created by parqueandroid on 22/1/18.
 */

public class Form_data {

    public String name ;
    public String label ;
    public String type ;
    public ArrayList<Option_Object> arrayList ;


    public Form_data(){

        name = new String();
        label = new String();
        type = new String();
        arrayList = new ArrayList();
    }
}
