package in.co.parque.task.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.co.parque.task.object_class.Option_Object;

/**
 * Created by parqueandroid on 23/1/18.
 */

public class Spinner_Adapter extends ArrayAdapter<Option_Object> {


    Context context ;
    ArrayList<Option_Object> values ;

    public Spinner_Adapter(Context context, int textViewResourceId,
                       ArrayList<Option_Object> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public Option_Object getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.DKGRAY);
        label.setPadding(20, 20, 20, 20);
        label.setText(values.get(position).value);
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.DKGRAY);
        label.setPadding(20, 20, 20, 20);
        label.setText(values.get(position).value);

        return label;
    }
}
